﻿2018-05-29
přítomni: Laďa, Zbyněk, Radomír, Marcel
agenda:
    • hlasování per rollam
    • refaktorizace webu
    • podpora jazykového korektoru - 
    • orientační sazebník
    • odměna Dana Koláře
    • členská schůze
    • projektová rada


zápis:
    • hlasování per rollam
    • refaktorizace webu PaRa
    • pro: Laďa, Ondřej, Radomír
    • proti: nikdo
    • zdrželi se: Marcel, Zbyněk
    • výsledek: přijato
    • podpora jazykového korektoru
    • pro: Laďa, Ondřej, Radomír, Zbyněk
    • proti: nikdo
    • zdržel se: nikdo
    • výsledek: přijato
    • orientační sazebník
    • Zbyněk doplní návrh 2018-05-30. Pokusíme se získat čís
    • odměna Dana Koláře - stále blokováno sazebníkem
    • členská schůze - Radomír ověří možnosti u nic. Paralelní hledání alternativ vítáno
    • Výbor nominuje následující kandidáty do projektové rady:
    • Jáchym Čepický | http://gismentors.cz/mentors/cepicky/
    • Ivor Kollár | http://hysteria.cz/niekt0/
    • Lukáš Jelínek | https://www.linuxexpres.cz/proc-pouziva-linux/lukas-jelinek
    • Václav Klecanda | web spolku | https://wiki.pirati.cz/lide/vaclav_klecanda
    • Jan Mareš | OpenAlt | https://openalt.cz/2016/program_detail.php#event_3101
    • Ivan Pavle | Město Domažlice  https://www.domazlice.eu/mestsky-urad/organizacni-struktura/odbory/osoba-ing-ivan-pavle-111.html
    • Marcel Kolaja
    • hlasování
    • pro: Laďa, Zbyněk, Radomír
    • proti: nikdo
    • zdržel se: Marcel

